document.addEventListener('DOMContentLoaded', function() {
    const tabsContainer = document.querySelector('.tabs-container');
    const tabContent = document.querySelector('.tab-content');

    tabsContainer.addEventListener('click', function(event) {
        if (event.target.classList.contains('tab')) {
            const tabId = event.target.getAttribute('data-tab');
            const activeTab = document.querySelector('.tab.active');
            const activeTabContent = document.querySelector('.tab-pane.active');

            if (activeTab) {
                activeTab.classList.remove('active');
            }

            if (activeTabContent) {
                activeTabContent.classList.remove('active');
            }

            event.target.classList.add('active');
            document.getElementById(tabId).classList.add('active');
        }
    });
});
